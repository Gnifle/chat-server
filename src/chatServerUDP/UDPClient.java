package chatServerUDP;

import java.io.IOException;
import java.net.*;

public class UDPClient {
	
	private static DatagramSocket datagramSocket;
	private static DatagramPacket outPacket;
	
	private static String message = "Det' en hjort!";
	private static InetAddress address;
	private static int port = 6780;

	public static void main(String[] args) {

		try {
			
			address = InetAddress.getLocalHost();
			datagramSocket = new DatagramSocket();
			
			outPacket = new DatagramPacket(message.getBytes(), message.length(), address, port);
			datagramSocket.send(outPacket);
			
			byte[] buffer = new byte[256];
			DatagramPacket inPacket = new DatagramPacket(buffer, buffer.length);
			
			while(inPacket.getData() == null) {
				System.out.println("Waiting...");
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			datagramSocket.receive(inPacket);
			
			String input = new String(inPacket.getData(), 0, inPacket.getLength());
			System.out.println("Message from server: " + input);
			
		} catch (SocketException e) {
			e.printStackTrace();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			datagramSocket.close();
			
		}
		
	}

}
