package chatServerUDP;

import java.io.IOException;
import java.net.*;

public class UDPserver {

	private static DatagramSocket datagramSocket;
	
	public static void main(String[] args) {
		
		try {
			datagramSocket = new DatagramSocket(6780);
			
			while(true) {
				
				byte[] buffer = new byte[256];
				DatagramPacket inPacket = new DatagramPacket(buffer, buffer.length);
				datagramSocket.receive(inPacket);
				
				String input = new String(inPacket.getData(), 0, inPacket.getLength());
				System.out.println("Message from client: " + input);
				
				String message = "Jamen dog! :O";
				int port = inPacket.getPort();
				InetAddress address = inPacket.getAddress();
				datagramSocket = new DatagramSocket();
				
				DatagramPacket outPacket = new DatagramPacket(message.getBytes(), message.length(), address, port);
				datagramSocket.send(outPacket);
				
			}
			
		} catch (SocketException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			datagramSocket.close();
			
		}
		
	}

}
