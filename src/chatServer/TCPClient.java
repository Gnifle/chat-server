package chatServer;

import java.net.*;
import java.util.ArrayList;
import java.io.*;

public class TCPClient implements Runnable {
	
	ArrayList<String> participants;
	
	private Chat chat;
	private DataController controller;
	
	private String host = getLocalhost();
	private int port = 6780;
	private String displayName;
	
	private Socket socket;
	private DataOutputStream dataOutputStream;

	public TCPClient(String serverIP, int port, String displayName, DataController controller) {
		
		this.host = serverIP;
		this.port = port;
		this.controller = controller;
		this.displayName = displayName;
		
	}
	
	@Override
	public void run() {
		
		controller.closeLauncher();
		
		chat = new Chat(this);
		Thread chatWindow = new Thread(chat);
		chatWindow.start();
		
		try {
			
			socket = getSocket();
			if(socket != null) {
				System.out.println("Client: Connected to: " + socket.getInetAddress().getHostName());
			} else {
				System.out.println("Socket error! Aborting!");
				System.exit(-1);
			}
			
			// Create data output stream
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
			dataOutputStream.writeUTF("////name " + displayName + "\n");
			
			// ObjectInputStream to receive participant arraylist from server
//			objectIn = new ObjectInputStream(socket.getInputStream());
//			getParticipantsFromServerAndUpdateGUI();
			
			// Read from console and network
			BufferedReader inputFromUser = new BufferedReader(new InputStreamReader(System.in));
			BufferedReader inputFromNetwork = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			String inputFromUserString = "";
			
			while(!inputFromUserString.equals("quit")) {
				
				// When console reader is ready, read the current console line and write the console input to output stream
				if(inputFromUser.ready()) {
					inputFromUserString = inputFromUser.readLine() + "\n";
					dataOutputStream.writeUTF(inputFromUserString.substring(0, inputFromUserString.indexOf("\n")));
					inputFromUserString = inputFromUserString.substring(0, inputFromUserString.indexOf("\n"));
				}
				
				// Sleep for 0.1 seconds to avoid clashing of ready() method from both user and network input
				Thread.sleep(100);
				
				// When network reader is ready, print message from network
				if(inputFromNetwork.ready()) {
					String receivedMessage = inputFromNetwork.readLine();
					chat.append(receivedMessage);
				}
				
			}
			
			
		// Error handling
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
			
		// Close socket when finished, close program on error
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("Could not close socket! Forcing exit!");
				System.exit(1);
			}
		}
	}
	
//	@SuppressWarnings({ "unchecked", "unused" })
//	private void getParticipantsFromServerAndUpdateGUI() {
//		try {
////			dataOutputStream.writeUTF("////getparticipants \n");
////			Thread.sleep(100);
//			participants = (ArrayList<String>) objectIn.readObject();
//			System.out.println(participants);
//			if(participants != null) {
//				chat.updateParticipants(participants);
//			}
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
////		} catch (InterruptedException e) {
////			e.printStackTrace();
//		}
//	}

	private static String getLocalhost() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	protected void sendToAll(String message) {
		try {
			dataOutputStream.writeUTF(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void exit() {
		try {
			dataOutputStream.writeUTF("quit");
			chat.exit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	// Returns a socket for either IP or localhost depending on 'useLocalhost' boolean
	private Socket getSocket() {
		try {
			return new Socket(host, port);
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Chat getChat() {
		return chat;
	}
	
	public DataController getController() {
		return controller;
	}

}
