package chatServer;

import java.io.*;

public class DataController {
	
	Launcher launcher;
	InputStream inputStream;
	
	private TCPClient client;
	
	public DataController() {
		this.client = null;
	}
	
	public DataController(TCPClient client) {
		this.client = client;
	}

	public void sendToAll(String text) {
		client.sendToAll(text);
	}
	
	public void exit() {
		client.exit();
	}
	
	public TCPClient getClient() {
		return client;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}

	public void startClient(String serverIP, int port, String displayName, DataController controller) {
		client = new TCPClient(serverIP, port, displayName, controller);
		Thread clientThread = new Thread(client);
		clientThread.start();
	}

	public void setLauncherReference(Launcher launcher) {
		this.launcher = launcher;
	}

	public void closeLauncher() {
		launcher.close();
	}

}
