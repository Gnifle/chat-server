package chatServer;

import java.net.*;
import java.io.*;
import java.util.*;


public class TCPServer {
	
	ServerSocket serverSocket;
	Socket socket;
	
	Hashtable<Socket, DataOutputStream> clientDataOutputStreams = new Hashtable<>();
//	Hashtable<Socket, ObjectOutputStream> clientObjectOutputStreams = new Hashtable<>();
//	Hashtable<Socket, String> clientNameList = new Hashtable<>();
	
	ArrayList<String> usernameList = new ArrayList<>(); 
	
	private static final int port = 6780;

	public static void main(String[] args) {
		new TCPServer();
	}
	
	public TCPServer() {
		try {
			
			// General server socket
			serverSocket = new ServerSocket(port);
			System.out.println("Server: Waiting for client...");
			
			while(true) {
				
				// Listen for client - blocks until reply is received with 3-way handshake
				socket = serverSocket.accept();
				
				// Create ClientHandler instance and run it on a seperate thread
				ClientHandler clientHandler = new ClientHandler(this, socket);
				Thread thread = new Thread(clientHandler);
				thread.start();
				
				// Create DataOutputStream and put it into a map
				DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
				clientDataOutputStreams.put(socket, dataOutputStream);

				// Create ObjectOutputStream and put it into a map
//				ObjectOutputStream objectOut = new ObjectOutputStream(socket.getOutputStream());
//				clientObjectOutputStreams.put(socket, objectOut);
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
			// Close server socket
			try {
				serverSocket.close();
			} catch (IOException e) {
				System.out.println("Could not close server socket! Forcing exit!");
				System.exit(1);
				
			}
		}
	}
	
	public void sendToAll(String message) {
		
		for(Enumeration<DataOutputStream> elements = clientDataOutputStreams.elements(); elements.hasMoreElements();) {
			
			DataOutputStream outputStream = elements.nextElement();
			
			try {
				outputStream.write(message.getBytes());
				outputStream.flush();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	public synchronized void removeClient(Socket socket) {
		clientDataOutputStreams.remove(socket);
//		clientObjectOutputStreams.remove(socket);
//		clientNameList.remove(socket);
		
		if(clientDataOutputStreams.size() == 0) {
			System.exit(0);
		}
	}

	public void addName(String value, Socket socket) {
		usernameList.add(value);
//		clientNameList.put(socket, value);
	}

//	public void getParticipants(Socket socket) {
//		try {
//			clientObjectOutputStreams.get(socket).writeObject(usernameList);
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
//	public void sendParticipants() {
//
//		for(Enumeration<ObjectOutputStream> elements = clientObjectOutputStreams.elements(); elements.hasMoreElements();) {
//			
//			ObjectOutputStream objectOut = elements.nextElement();
//			
//			try {
//				objectOut.writeObject(usernameList);
//				objectOut.flush();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			
//		}
//		
//	}

}
