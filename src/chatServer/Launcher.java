package chatServer;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;

import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Launcher implements Runnable {
	
	private DataController controller = new DataController();
	
	private JFrame frmChatLauncher;
	private JTextField tfDisplayName;
	private JTextField tfIP1;
	private JTextField tfIP2;
	private JTextField tfIP3;
	private JTextField tfIP4;
	private JTextField tfServerPort;
	private JLabel lblDot1;
	private JLabel lblDot2;
	private JLabel lblDot3;
	private JCheckBox cbLocalhost;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Launcher window = new Launcher();
					window.frmChatLauncher.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Launcher() {
		
		try	{
			UIManager.setLookAndFeel(new SyntheticaBlackEyeLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmChatLauncher = new JFrame();
		frmChatLauncher.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent wc) {
				if(confirmCancel() == 0) {
					System.exit(0);
				}
			}
		});
		frmChatLauncher.setTitle("Chat Launcher");
		frmChatLauncher.setBounds(100, 100, 310, 323);
		frmChatLauncher.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmChatLauncher.getContentPane().setLayout(null);
		frmChatLauncher.setResizable(false);
		
		JLabel lblDisplayName = new JLabel("Display name");
		lblDisplayName.setBounds(10, 15, 284, 14);
		frmChatLauncher.getContentPane().add(lblDisplayName);
		
		tfDisplayName = new JTextField();
		tfDisplayName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		tfDisplayName.setBounds(10, 35, 274, 30);
		tfDisplayName.setToolTipText("<html>Enter a name used to identify yourself on the server.<br>This name will be visible to others on the same server.</html>");
		frmChatLauncher.getContentPane().add(tfDisplayName);
		tfDisplayName.setColumns(10);
		
		JLabel lblLocalhost = new JLabel("Use localhost?");
		lblLocalhost.setBounds(10, 76, 71, 14);
		lblLocalhost.setToolTipText("<html>Check if you want to use localhost for this chat or leave unchecked to enter a server IP and host.<br>If localhost is selected, you do not need to enter any server IP or host number.</html>");
		frmChatLauncher.getContentPane().add(lblLocalhost);
		
		cbLocalhost = new JCheckBox();
		cbLocalhost.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent i) {
				if(cbLocalhost.isSelected()) {
					tfIP1.setEnabled(false);
					tfIP2.setEnabled(false);
					tfIP3.setEnabled(false);
					tfIP4.setEnabled(false);
				} else {
					tfIP1.setEnabled(true);
					tfIP2.setEnabled(true);
					tfIP3.setEnabled(true);
					tfIP4.setEnabled(true);
				}
			}
		});
		cbLocalhost.setBounds(80, 72, 29, 23);
		cbLocalhost.setToolTipText("<html>Check if you want to use localhost for this chat or leave unchecked to enter a server IP and host.<br>If localhost is selected, you do not need to enter any server IP or host number.</html>");
		frmChatLauncher.getContentPane().add(cbLocalhost);
		
		JLabel lblServerIpAddress = new JLabel("Server IP Address");
		lblServerIpAddress.setBounds(10, 101, 284, 14);
		frmChatLauncher.getContentPane().add(lblServerIpAddress);
		
		tfIP1 = new JTextField();
		tfIP1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		tfIP1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent k) {
				
				if(tfIP1.getText().length() > 3) {
					tfIP1.setBackground(new Color(255, 60, 80));
				} else {
					tfIP1.setBackground(tfDisplayName.getBackground());
				}
				
				if(tfIP1.getText().length() == 3) {
					tfIP2.requestFocusInWindow();
				}
					
			}
		});
		tfIP1.setBounds(10, 121, 61, 30);
		tfIP1.setToolTipText("Enter the server IP you wish to connect to");
		frmChatLauncher.getContentPane().add(tfIP1);
		tfIP1.setColumns(10);
		
		tfIP2 = new JTextField();
		tfIP2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		tfIP2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent k) {
				
				if(tfIP2.getText().length() > 3) {
					tfIP2.setBackground(new Color(255, 60, 80));
				} else {
					tfIP2.setBackground(tfDisplayName.getBackground());
				}
				
				if(tfIP2.getText().length() == 3) {
					tfIP3.requestFocusInWindow();
				}
					
			}
		});
		tfIP2.setColumns(10);
		tfIP2.setBounds(81, 121, 61, 30);
		tfIP2.setToolTipText("Enter the server IP you wish to connect to");
		frmChatLauncher.getContentPane().add(tfIP2);
		
		tfIP3 = new JTextField();
		tfIP3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		tfIP3.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent k) {
				
				if(tfIP3.getText().length() > 3) {
					tfIP3.setBackground(new Color(255, 60, 80));
				} else {
					tfIP3.setBackground(tfDisplayName.getBackground());
				}
				
				if(tfIP3.getText().length() == 3) {
					tfIP4.requestFocusInWindow();
				}
					
			}
		});
		tfIP3.setColumns(10);
		tfIP3.setBounds(152, 121, 61, 30);
		tfIP3.setToolTipText("Enter the server IP you wish to connect to");
		frmChatLauncher.getContentPane().add(tfIP3);
		
		tfIP4 = new JTextField();
		tfIP4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		tfIP4.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent k) {
				
				if(tfIP4.getText().length() > 3) {
					tfIP4.setBackground(new Color(255, 60, 80));
				} else {
					tfIP4.setBackground(tfDisplayName.getBackground());
				}
				
				if(tfIP4.getText().length() == 3) {
					tfServerPort.requestFocusInWindow();
				}
					
			}
		});
		tfIP4.setColumns(10);
		tfIP4.setBounds(223, 121, 61, 30);
		tfIP4.setToolTipText("Enter the server IP you wish to connect to");
		frmChatLauncher.getContentPane().add(tfIP4);
		
		JLabel lblServerPort = new JLabel("Server port");
		lblServerPort.setBounds(10, 162, 284, 14);
		frmChatLauncher.getContentPane().add(lblServerPort);
		
		tfServerPort = new JTextField();
		tfServerPort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connect();
			}
		});
		tfServerPort.setColumns(10);
		tfServerPort.setBounds(10, 181, 132, 30);
		tfServerPort.setToolTipText("Enter the host number on the given server IP you wish to connect to");
		tfServerPort.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent k) {
				
				if(tfServerPort.getText().length() > 5) {
					tfServerPort.setBackground(new Color(255, 60, 80));
				} else {
					tfServerPort.setBackground(tfDisplayName.getBackground());
				}
					
			}
		});
		frmChatLauncher.getContentPane().add(tfServerPort);
		
			// Dots between IP address fields
			
			lblDot1 = new JLabel(".");
			lblDot1.setHorizontalAlignment(SwingConstants.CENTER);
			lblDot1.setVerticalAlignment(SwingConstants.BOTTOM);
			lblDot1.setBounds(71, 121, 10, 30);
			frmChatLauncher.getContentPane().add(lblDot1);
			
			lblDot2 = new JLabel(".");
			lblDot2.setVerticalAlignment(SwingConstants.BOTTOM);
			lblDot2.setHorizontalAlignment(SwingConstants.CENTER);
			lblDot2.setBounds(142, 121, 10, 30);
			frmChatLauncher.getContentPane().add(lblDot2);
			
			lblDot3 = new JLabel(".");
			lblDot3.setVerticalAlignment(SwingConstants.BOTTOM);
			lblDot3.setHorizontalAlignment(SwingConstants.CENTER);
			lblDot3.setBounds(214, 121, 10, 30);
			frmChatLauncher.getContentPane().add(lblDot3);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int confirmCancel = JOptionPane.showConfirmDialog(null, "Would you like exit?");
				
				if(confirmCancel == 0) {
					System.exit(0);
				}
			}
		});
		btnCancel.setBounds(10, 227, 132, 36);
		btnCancel.setToolTipText("Cancel connection and end launcher");
		frmChatLauncher.getContentPane().add(btnCancel);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				connect();
			}
		});
		btnConnect.setBounds(152, 227, 132, 36);
		btnConnect.setToolTipText("Start connection to server");
		frmChatLauncher.getContentPane().add(btnConnect);
		
		frmChatLauncher.setVisible(true);
	}
	
	protected int confirmCancel() {
		int confirmCancel = JOptionPane.showConfirmDialog(null, "Would you like exit?");
		return confirmCancel;
	}
	
	public void connect() {
		if(fieldsAreEntered(!(cbLocalhost.isSelected())) && fieldsFormatCorrect(!(cbLocalhost.isSelected()))) {
			
			int serverPort = Integer.parseInt(tfServerPort.getText());
			String serverIP = null;
			String displayName = tfDisplayName.getText();
			
			if(!(cbLocalhost.isSelected())) {
				serverIP = tfIP1.getText() + "." + tfIP2.getText() + "." + tfIP3.getText() + "." + tfIP4.getText();
			} else {
				
				try {
					serverIP = InetAddress.getLocalHost().getHostAddress();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				
			}
			
			startAndConnect(serverIP, serverPort, displayName);
		}
	}

	private boolean fieldsAreEntered(boolean checkIP) {
		
		if(checkIP) {
			if(
				!(tfDisplayName.getText().length() > 0) ||
				!(tfIP1.getText().length() > 0) ||
				!(tfIP2.getText().length() > 0) ||
				!(tfIP3.getText().length() > 0) ||
				!(tfIP4.getText().length() > 0) ||
				!(tfServerPort.getText().length() > 0)
				) {
				
				JOptionPane.showMessageDialog(null, "<html>Display name, server IP and server port <b>must</b> be entered.</html>");
				return false; // Return false if any validation fails
			}
		} else {
			if(
				!(tfDisplayName.getText().length() > 0) ||
				!(tfServerPort.getText().length() > 0)
				) {
				
				JOptionPane.showMessageDialog(null, "<html>Display name and localhost server port <b>must</b> be entered.</html>");
				return false; // Return false if any validation fails
			}
		}
		
		return true; // Return true is validation runs
	}
	
	private boolean fieldsFormatCorrect(boolean checkIP) {
		
		try {
			float ip1, ip2, ip3, ip4, port;
			
			if(checkIP) {
				ip1 = Float.parseFloat(tfIP1.getText());
				ip2 = Float.parseFloat(tfIP2.getText());
				ip3 = Float.parseFloat(tfIP3.getText());
				ip4 = Float.parseFloat(tfIP4.getText());
			}
			port = Float.parseFloat(tfServerPort.getText());
			
		} catch (NumberFormatException nfe) {
			JOptionPane.showMessageDialog(null, "Please enter numbers only");
			return false;
		}
		
		return true;
	}
	
	private void startAndConnect(String serverIP, int port, String displayName) {
		if(serverIP != null) {
			controller.setLauncherReference(this);
			controller.startClient(serverIP, port, displayName, controller);
		}
	}

	public void close() {
		frmChatLauncher.dispose();
	}

	@Override
	public void run() {
		initialize();
	}
}
