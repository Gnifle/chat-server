package chatServer;

import java.net.*;
import java.io.*;

public class ClientHandler implements Runnable {
	
	TCPServer server;
	Socket socket;
	
	public ClientHandler(TCPServer server, Socket socket) {
		this.server = server;
		this.socket = socket;
	}

	@Override
	public void run() {
		
		try {
			
			DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
			String input = "";
			
			while(!input.equals("quit")) {
				input = dataInputStream.readUTF();
				
				if(input.contains("////")) {
					serverCommand(input);
				} else {
					server.sendToAll(input + "\n");
				}
				
//				server.sendParticipants();
				
			}
			
			server.removeClient(socket);
			socket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
	}

	private void serverCommand(String input) {
		String command = input.substring(input.indexOf("////") + 4, input.indexOf(' '));
		String value = input.substring(input.indexOf(' ') + 1, input.indexOf("\n"));
		
		switch (command) {
			case "name":
				server.addName(value, socket);
				break;

//			case "getparticipants":
//				server.getParticipants(socket);
//				break;
				
			default:
				break;
		}
	}
}
