package chatServer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;
import javax.swing.border.EmptyBorder;

public class Chat implements Runnable {
	
	private TCPClient client;
	private DataController controller;

	private JFrame frmHedgehogChat;
	private JTextField textFieldInput;
	private JTextArea textAreaChat;
	
	private DefaultListModel<String> listModel;
	
	/**
	 * <i>Chat</i> is the GUI setup for the chat window to use as client for the server.
	 * It is launched after opening and filling in the Launcher, where a server IP,
	 * server port and chat nickname is entered.<br><br>
	 * A TCPClient object is created from these given paramters and passed to Chat.
	 * @param client The TCPClient object passed after connecting to a server
	 */
	public Chat(TCPClient client) {
		
		this.client = client;
		this.controller = client.getController();
		
		try	{
			UIManager.setLookAndFeel(new SyntheticaBlackEyeLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHedgehogChat = new JFrame();
		frmHedgehogChat.setIconImage(Toolkit.getDefaultToolkit().getImage(Chat.class.getResource("/chatServer/icon.ico")));
		frmHedgehogChat.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				controller.exit();
			}
		});
		frmHedgehogChat.setTitle("Gnifle's chat");
		frmHedgehogChat.setBounds(100, 100, 600, 400);
		frmHedgehogChat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		// Menubar
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(UIManager.getColor("CheckBox.foreground"));
		frmHedgehogChat.getContentPane().add(menuBar, BorderLayout.NORTH);
		
			JMenu mnFile = new JMenu("File");
			menuBar.add(mnFile);
			
				JMenuItem mntmConnect = new JMenuItem("Connect");
				mnFile.add(mntmConnect);
				KeyStroke ksConnect = KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK);
				mntmConnect.setAccelerator(ksConnect);
				
				JMenuItem mntmDisconnect = new JMenuItem("Disconnect");
				mnFile.add(mntmDisconnect);
				KeyStroke ksDisconnect = KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK);
				mntmDisconnect.setAccelerator(ksDisconnect);
				
				JMenuItem mntmExit = new JMenuItem("Exit");
				mnFile.add(mntmExit);
				KeyStroke ksExit = KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_DOWN_MASK);
				mntmExit.setAccelerator(ksExit);
			
			JMenu mnEdit = new JMenu("Edit");
			menuBar.add(mnEdit);
			
				JMenuItem mntmPreferences = new JMenuItem("Preferences");
				mnEdit.add(mntmPreferences);
				KeyStroke ksPreferences = KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK);
				mntmPreferences.setAccelerator(ksPreferences);
			
			JMenu mnHelp = new JMenu("Help");
			menuBar.add(mnHelp);
			
				JMenuItem mntmAbout = new JMenuItem("About");
				mnHelp.add(mntmAbout);
				
		// Chat panel
				
		JPanel panelChat = new JPanel();
		panelChat.setBorder(new EmptyBorder(5, 3, 5, 4));
		frmHedgehogChat.getContentPane().add(panelChat, BorderLayout.CENTER);
		panelChat.setLayout(new BorderLayout(0, 0));
		
			JScrollPane scrollPaneChat = new JScrollPane();
			panelChat.add(scrollPaneChat, BorderLayout.CENTER);
			
				textAreaChat = new JTextArea();
				textAreaChat.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent e) {
						textAreaChat.setFocusable(false);
					}
				});
				textAreaChat.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent e) {
						textAreaChat.setFocusable(true);
					}
				});
				textAreaChat.setEditable(false);
				textAreaChat.setFocusable(false);
				scrollPaneChat.setViewportView(textAreaChat);
		
		// Input panel
		
		JPanel panelInput = new JPanel();
		panelInput.setBorder(new EmptyBorder(0, 2, 5, 2));
		panelInput.setPreferredSize(new Dimension(frmHedgehogChat.getWidth(), 40));
		frmHedgehogChat.getContentPane().add(panelInput, BorderLayout.SOUTH);
		panelInput.setLayout(new BorderLayout(0, 0));
		
			// Send button
		
			JButton btnSend = new JButton("Send");
			btnSend.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					post();
				}
			});
			panelInput.add(btnSend, BorderLayout.EAST);
			
			// Input field
			
			JPanel panelInputField = new JPanel();
			panelInputField.setBorder(new EmptyBorder(2, 0, 2, 0));
			panelInput.add(panelInputField, BorderLayout.CENTER);
			panelInputField.setLayout(new BorderLayout(0, 0));
			
				textFieldInput = new JTextField();
				textFieldInput.requestFocusInWindow();
				textFieldInput.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						post();
					}
				});
				panelInputField.add(textFieldInput);
				textFieldInput.setColumns(10);
		
		// Participants panel
		
		JPanel panelParticipants = new JPanel();
		panelParticipants.setBorder(new EmptyBorder(5, 0, 5, 5));
		panelParticipants.setPreferredSize(new Dimension(200, panelParticipants.getHeight()));
		frmHedgehogChat.getContentPane().add(panelParticipants, BorderLayout.EAST);
		panelParticipants.setLayout(new BorderLayout(0, 0));
		
			JScrollPane scrollPaneParticipants = new JScrollPane();
			panelParticipants.add(scrollPaneParticipants);
			
				// Participant list
			
				listModel = new DefaultListModel<String>();
				
				JList<String> listParticipants = new JList<String>(listModel);
				listParticipants.setFocusable(false);
				scrollPaneParticipants.setViewportView(listParticipants);
				
				// Participants header
				
				JPanel panelParticipantHeader = new JPanel();
				panelParticipantHeader.setBorder(new EmptyBorder(2, 2, 5, 2));
				panelParticipants.add(panelParticipantHeader, BorderLayout.NORTH);
				panelParticipantHeader.setLayout(new BorderLayout(0, 0));
				
				JLabel lblParticipants = new JLabel("Participants");
				lblParticipants.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panelParticipantHeader.add(lblParticipants);
		
		frmHedgehogChat.setVisible(true);
		
	}

	/**
	 * Stores the text from the input field and sends it to the server.<br>
	 * If the text is '<i>quit</i>', the client will exit.
	 */
	protected void post() {
		if(textFieldInput.getText().length() > 0) {
			if(!textFieldInput.getText().equals("quit")) {
				controller.sendToAll(client.getDisplayName() + ": " + textFieldInput.getText());
				textFieldInput.setText("");
			} else {
				exit();
				System.exit(0);
			}
		}
	}
	
	/**
	 * Accepts a String as input, containing a message from the server to be posted in all clients.<br>
	 * The method is triggered by the clients InputListener.<br><br>
	 * After appending the message to the rest of the chat, the caret is repositioned to the very end
	 * of the scrollpane for the chat, so that the newest messages are always displayed.
	 * @param message The message sent from the server to be posted in the client
	 */
	protected void append(String message) {
		textAreaChat.append(message + "\n");
		textAreaChat.setCaretPosition(textAreaChat.getText().length());
	}
	
//	protected void updateParticipants(ArrayList<String> participants) {
//
//		if(listModel != null) {
//			listModel.clear();
//			
//			for (String user : participants) {
//				listModel.addElement(user);
//			}
//		}
//	}
	
	/**
	 * Closes the chat window
	 */
	protected void exit() {
		frmHedgehogChat.dispose();
	}

	/**
	 * Returns the TCPClient object passed to the Chat object on instantiation
	 * @return the TCPClient object passed to the Chat object on instantiation
	 */
	public TCPClient getClient() {
		return client;
	}

	/**
	 * @wbp.parser.entryPoint <br>Let's Eclipse GUI builder know what part of the code to use as entry point to parse the layout
	 */
	@Override
	public void run() {
		initialize();
	}

}
