package nanoTCP;

import java.io.*;
import java.net.*;

public class NanoTCPServer extends Thread {
	private static String host = "10.111.180.75";
	private static int port = 6780;
	private static ServerSocket serverSocket;
	private static Socket socket;
	private static InputStream inputStream;
	private static InputStreamReader inputStreamReader;
	
	public static ChatWindow chatWindow;
	
	@SuppressWarnings("static-access")
	public NanoTCPServer(ChatWindow chatWindow) {
		this.chatWindow = chatWindow;
	}
	
	@Override
	public void run() {
		server();
	}

	public static void server() {
		System.out.println("Server waiting for client...");
		
		try {
			
			serverSocket = new ServerSocket(port);
			
			while(true) {
				socket = serverSocket.accept();
				
				inputStream = socket.getInputStream();
				inputStreamReader = new InputStreamReader(inputStream, "iso-8859-1");
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				
				chatWindow.receive(bufferedReader.readLine());
			}
			
		} catch (IOException e) {
			System.out.println("Socket is closed");
			e.printStackTrace();
		}
	}

}
