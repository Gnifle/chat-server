package nanoTCP;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.*;

import javax.swing.JTextArea;
import javax.swing.UIManager;

public class ChatWindow {

	public JFrame frame;
	public JTextField textField_1;
	public JTextArea textArea;
	
	private static String host = "10.111.180.75";
	private static int port = 6780;
	
	public NanoTCPServer server;
	private static Socket socket;
	private static OutputStream outputStream;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
	}

	/**
	 * Create the application.
	 */
	public ChatWindow() {
		try {
//			host = InetAddress.getLocalHost().getHostAddress();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		initialize();
		
		server = new NanoTCPServer(this);
		server.start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 350, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Ze Chat Program");
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setBorder(new EmptyBorder(0, 0, 0, 0));
		textArea.setEditable(false);
		textArea.setBackground(UIManager.getColor("Button.background"));
		scrollPane.setViewportView(textArea);
		
		JPanel chatCommands = new JPanel();
		chatCommands.setPreferredSize(new Dimension(frame.getWidth(), 30));
		frame.getContentPane().add(chatCommands, BorderLayout.SOUTH);
		chatCommands.setLayout(new BorderLayout(0, 0));
		
		JButton btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				post(textField_1, textArea);
			}
		});
		chatCommands.add(btnSend, BorderLayout.EAST);
		
		textField_1 = new JTextField();
		textField_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				post(textField_1, textArea);
			}
		});
		chatCommands.add(textField_1, BorderLayout.CENTER);
		textField_1.setColumns(10);
	}

	protected void post(JTextField textField, JTextArea destination) {
		String text = textField.getText();
		destination.append(text + "\n");
		textField.setText("");
		
		try {
			socket = new Socket(host, port);
			outputStream = socket.getOutputStream();
			DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
			
			dataOutputStream.writeBytes(text);

		} catch (UnknownHostException uhEx) {
			
			uhEx.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				System.exit(1);
			}
		}
	}
	
	public void receive(String text) {
		textArea.append(text + "\n");
	}

}
