package nanoTCP;

import java.io.*;
import java.net.*;
import java.util.Hashtable;

public class ChatServer {
	
	private ServerSocket serverSocket = null;
	private int port = 6780;
	Hashtable<Socket, DataOutputStream> outStreams = new Hashtable<Socket, DataOutputStream>();

	public static void main(String[] args) {
		new ChatServer();
	}
	
	public ChatServer() {
		try {
			serverSocket = new ServerSocket(port);
			
			do {
				Socket socket = serverSocket.accept();
				
				DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
				outStreams.put(socket, dataOutputStream);
				ClientHandler handler = new ClientHandler(socket, this);
				
			} while (true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
