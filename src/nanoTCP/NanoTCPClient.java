package nanoTCP;

import java.io.*;
import java.net.*;

public class NanoTCPClient extends Thread {
	private static String host = "10.111.180.75";
	private static int port = 6780;
	
	private static Socket socket;
	private static OutputStream outputStream;
	
	@Override
	public void start() {
		client();
	}
	
	public static void main(String[] args) {
		client();
	}

	public static void client() {
		try {
			
			socket = new Socket(host, port);
			outputStream = socket.getOutputStream();
			DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
//			outputStream.write(98);
//			dataOutputStream.writeChars("�");
			
		} catch (UnknownHostException uhEx) {
			
			uhEx.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				System.exit(1);
			}
		}
	}

}
