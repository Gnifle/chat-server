package objectSocketTransfer;

import java.io.*;

public class Serializer {
	
	private static ObjectOutputStream objectOut;
	private static ObjectInputStream objectIn;
	private static SerialTest serialTest;

	public static void main(String[] args) {
		
		try {
			serialize(new FileOutputStream("serialTest.ser"));
			deserialize(new FileInputStream("serialTest.ser"));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public static void serialize(OutputStream outputStream) {
		serialTest = new SerialTest();
		
		try {
			objectOut = new ObjectOutputStream(outputStream);
			objectOut.writeObject(serialTest);
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {
				objectOut.close();
				outputStream.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void deserialize(FileInputStream fileInputStream) {
		
		SerialTest receivedSerialTest = null;
		
		try {
			objectIn = new ObjectInputStream(fileInputStream);
			serialTest = (SerialTest) objectIn.readObject();
			
			System.out.println(serialTest.version);
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} finally {
			try {
				objectIn.close();
				fileInputStream.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}


}
